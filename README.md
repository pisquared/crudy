# CRUDy
*Create, Retrieve, Update, Delete - Y not?*

Framework to CRUD items by standard REST-ful APIs while minimizing remote communication and update seamlessly.

* Easy way to display, create, edit and remove elements from a collection. Wrapper around [Backbone](http://backbonejs.org/)
* Cache retrieved data in LocalStorage so that collections are not fetched on every page load, thanks to [Backbone.LocalStorage](http://backbonejs.org/docs/backbone.localStorage.html)
* Listen for updates via [socket.io](https://github.com/socketio/socket.io-client) when a remote element is created, updated or deleted, update the local cache and display results
* Common editable elements - **text**, **textarea**, **date/time** with [bootstrap-datetimepicker](https://github.com/Eonasdan/bootstrap-datetimepicker), **select** with [bootstrap-select](https://silviomoreto.github.io/bootstrap-select/)
* Rearrange items that are *orderable* with [dragula](https://bevacqua.github.io/dragula/)

## Integrated with [shipka](https://gitlab.com/pisquared/shipka)