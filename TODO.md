# TODO:
* Sync version instead of time
* Add service-workers support (see [crudy_shipka](https://gitlab.com/pisquared/crudy_shipka) for initial working prototype)
  - disconnect sockets if offline
  - write locally
  - sync upon online
* On conflicting versions in offline sync:
  - Say it's a conflict and allow the user to respond
* Disallow creating empty body
* UNDO functionality for deletion - possible integration with [shipka](https://gitlab.com/pisquared/shipka)
* When editing, default fields should also be updated, right now they are empty
* Conditional sync on initial fetch - don't fetch everything
* Fetch via websockets
* [BUG] waiting for https://github.com/silviomoreto/bootstrap-select/issues/1135 Bootstrap-select for bootstrap 4 fix
