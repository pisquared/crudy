var oldSync = Backbone.ajaxSync;
Backbone.ajaxSync = function (method, model, options) {
  options.beforeSend = function (xhr) {
    xhr.setRequestHeader('X-CSRFToken', CSRF_TOKEN);
  };
  return oldSync(method, model, options);
};

var NOTIFIER = {};
var COLLECTIONS = {};
var DT_STATE = {};
var EVENT_BUS = _({}).extend(Backbone.Events);
var REQUEST_SID = "not_set";
_.extend(NOTIFIER, Backbone.Events);

/** Datetime picker icons with font-awesome */
var DT_ICONS = {
  time: 'fa fa-clock',
  date: 'fa fa-calendar-alt',
  up: 'fa fa-chevron-up',
  down: 'fa fa-chevron-down',
  previous: 'fa fa-chevron-left',
  next: 'fa fa-chevron-right',
  today: 'fa fa-calendar',
  clear: 'fa fa-trash-alt',
  close: 'fa fa-times-circle'
};

function getObjectByString(o, s) {
  s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
  s = s.replace(/^\./, '');           // strip a leading dot
  var a = s.split('.');
  for (var i = 0, n = a.length; i < n; ++i) {
    var k = a[i];
    if (k in o) {
      o = o[k];
    } else {
      return;
    }
  }
  return o;
}

String.prototype.toSnakeCase = function () {
  var upperChars = this.match(/([A-Z])/g);
  if (!upperChars) {
    return this;
  }

  var str = this.toString();
  for (var i = 0, n = upperChars.length; i < n; i++) {
    str = str.replace(new RegExp(upperChars[i]), '_' + upperChars[i].toLowerCase());
  }

  if (str.slice(0, 1) === '_') {
    str = str.slice(1);
  }

  return str;
};

function triggerEnterOn13(ctx) {
  ctx.$('textarea').keyup(function (e) {
    if (e.keyCode === 13 && !e.shiftKey) {
      $(this).trigger('enter');
    }
  });
}

function at(dateStr, exec) {
  console.log(dateStr);
  var eta_ms = new Date(dateStr).getTime() - Date.now();
  var timeout = setTimeout(exec, eta_ms);
}

function flash(message, category) {
  if (category === undefined)
    category = 'danger';
  var flashTemplate = _.template($("#fe-template-core-flash").html());
  var html = flashTemplate({
    data:
      {
        message: message,
        category: category
      }
  });
  $('#flashed-messages-container').html(html);
  if (category === 'success') {
    $(".alert").fadeTo(2000, 500).slideUp(500, function () {
      $(".alert").slideUp(500);
    });
  }
}


var ApplicationRegistry = (function () {
  /**
   * Register your applications here
   * @type {{}}
   */
  var apps = {};
  var socket;

  /**
   * Initialize the application registry with websockets
   */
  var init = function () {
    var webSocketsAvailable = false;
    if ("WebSocket" in window) {
      socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port);
      socket.on('connect', function () {
        var payload = {tz_offset_minutes: new Date().getTimezoneOffset(), address: location.pathname};
        if (DEBUG)
          console.log('> components_syn: ' + JSON.stringify(payload));
        socket.emit('components_syn', payload);
      });
      socket.on('components_ack', function (response) {
        if (DEBUG)
          console.log('< components_ack: ' + JSON.stringify(response));
        // acquired response, mark sockets available
        webSocketsAvailable = true;
        if ($.isEmptyObject(response.length)) {
          REQUEST_SID = response['request_sid'];
          updateFromServer(response);
        }
      });
    }
  };

  /**
   * What is the latest time that this appName has been updated
   * @param appName
   */
  var _getLatestLocalUpdate = function (appName) {
    var localStorageName = appName + "-collection-retrieved";
    return localStorage.getItem(localStorageName);
  };

  /**
   * Update the latest time an appName has been updated
   * @param appName
   */
  var _updateLatestUpdate = function (appName) {
    var localStorageName = appName + "-collection-retrieved";
    localStorage.setItem(localStorageName, Math.floor(Date.now() / 1000));
  };

  /**
   * Callback from a socket to update apps from init socket
   * @param socketResponse
   */
  var updateFromServer = function (socketResponse) {
    $.each(apps, function (appName, app) {
      if (socketResponse.models_updates === undefined)
        return;
      var modelUpdate = socketResponse.models_updates[appName];
      if (!modelUpdate)
        return;
      var latestFromServer = modelUpdate.latest;
      var latestLocal = _getLatestLocalUpdate(appName);
      if (!latestLocal || latestFromServer > latestLocal) {
        // the server has been updated later than current date
        app.collection.fetch({
          ajaxSync: true,
          success: function () {
            _updateLatestUpdate(appName);
            app.collection.each(function (model) {
              model.save();
            });
          }
        });
      }
    });
  };


  /**
   * Register created, updated, deleted socket events
   * @param appName
   */
  var registerSocketEvents = function (appName) {
    $.each(['created', 'updated', 'deleted'], function (_, eventName) {
      socket.on(appName + '.' + eventName, function (response) {
        var request_sid = response['request_sid'];
        if (DEBUG)
          console.log(appName + '_' + eventName, 'fromMe?:', request_sid === REQUEST_SID, response);
        if (request_sid !== REQUEST_SID)
          NOTIFIER.trigger("server_" + eventName + ":" + appName, response);
      });
    })
  };

  // ******************************************************************
  // initialize the registry
  init();

  // ******************************************************************
  // public interfaces
  return {
    /**
     * Register an app with a name
     * @param name
     * @param app
     */
    register: function (name, app) {
      apps[name] = app;
      app.collection.fetch();
      registerSocketEvents(name);
    },

    getApp: function (name) {
      return apps[name];
    },

    /**
     * What is the latest time that this appName has been updated
     * @param appName
     */
    getLatestLocalUpdate: function (appName) {
      _getLatestLocalUpdate(appName);
    },

    /**
     * Update the latest time an appName has been updated
     * @param appName
     */
    updateLatestUpdate: function (appName) {
      _updateLatestUpdate(appName);
    },

    /**
     * Fetch all periods for an app
     * @param start
     * @param end
     */
    getPeriod: function (start, end) {
      var data = {};
      if (start)
        data['start_ts'] = start;
      if (end)
        data['end_ts'] = end;
      $.each(apps, function (appName, app) {
        if (app.views) {
          $.each(app.views, function (_, view) {
            if (view.updatePeriod) {
              view.updatePeriod(start, end);
            }
          })
        }
        app.collection.fetch({
          ajaxSync: true,
          data: data
        })
      });
    }
  }
})();

var EditableInputElement = Backbone.View.extend({
  events: function () {
    return {
      'keyup .edit-entry': 'onUpdate',
      'click .edit-entry': 'onUpdate',
      "dp.change .edit-dt-entry": "onUpdate"
    }
  },

  initialize: function () {
    this.templateBody = _.template($("#fe-template-core-editable-input-body").html());
    this.templateTitle = _.template($("#fe-template-core-editable-input-title").html());
    this.templateStr = _.template($("#fe-template-core-editable-input-str").html());
    this.templateInt = _.template($("#fe-template-core-editable-input-int").html());
    this.templateDt = _.template($("#fe-template-core-editable-dt").html());
    this.templateSelect = _.template($("#fe-template-core-editable-select").html());
    this.realModel = this.model.attributes.realModel;
    this.render();
  },

  onUpdate: function () {
    var attr = this.$('.edit-entry').attr('name');
    EVENT_BUS.trigger('updateAttribute', attr);

    // update DT
    if (this.$('.edit-entry').hasClass('dt'))
      this.onDTUpdate(attr);
    // if (this.model.attributes.elDetails.effective_type === 'body')

    var newBody = this.$('.edit-entry').val();
    this.realModel.set(attr, newBody);
  },

  onDTUpdate: function (attr) {
    var d = this.$('.d-value').val();
    var t = this.$('.t-value').val();
    var z = this.$('.z-value').val();
    this.$('.d-value').attr('value', d);
    this.$('.t-value').attr('value', t);
    var newDt = d + ' ' + t + '' + z;
    this.$('.edit-entry').val(newDt);
    // propagate the event from the picker
    EVENT_BUS.trigger('updateAttributeDate', {from: attr, dt: newDt});
  },

  render: function () {
    var elDetails = this.model.attributes.elDetails;
    var html = "";
    if (elDetails.effective_type === 'title') {
      html = this.templateTitle(this.model.toJSON());
    } else if (elDetails.effective_type === 'body' || elDetails.effective_type === 'str_long') {
      html = this.templateBody(this.model.toJSON());
    } else if (elDetails.effective_type === 'int') {
      html = this.templateInt(this.model.toJSON());
    } else if (elDetails.effective_type === 'dt') {
      this.convertDtValue();
      html = this.templateDt(this.model.toJSON());
    } else if (elDetails.effective_type === 'select') {
      html = this.templateSelect(this.model.toJSON());
    } else if (elDetails.effective_type === 'select_model') {
      html = this.fetchSelectModel();
    } else {
      html = this.templateStr(this.model.toJSON());
    }
    this.$el.html(html);
    if (elDetails.effective_type === 'dt') {
      this.renderDt(this.model.get('name'));
    }
  },

  renderSelectModel: function (collection) {
    var elDetails = this.model.get('elDetails');
    var choices = [];
    collection.each(function (model) {
      choices.push([model.id, model.get('name')])
    });
    this.model.set('elDetails', _.extend(elDetails, {
      'choices': choices
    }));
    var html = this.templateSelect(this.model.toJSON());
    this.$el.html(html);
  },

  fetchSelectModel: function () {
    var elDetails = this.model.get('elDetails');
    var collection = COLLECTIONS[elDetails.model];

    this.listenTo(collection, 'sync', function () {
      this.renderSelectModel(collection)
    }.bind(this));
    if (collection.length)
      return this.renderSelectModel(collection);
  },

  convertDtValue: function () {
    var dtValue = this.model.get('value');
    if (!dtValue) {
      this.model.set('value', {
        'dt': dtValue,
        'd': null,
        't': null,
        'z': USER_TIMEZONE
      });
    } else {
      this.model.set('value', {
        'dt': dtValue,
        'd': moment(dtValue).format("YYYY-MM-DD"),
        't': moment(dtValue).format("HH:mm"),
        'z': moment(dtValue).format("Z")
      });
    }
    var bind = this.$el.data('bind');
    if (bind) {
      this.bind = bind;
      if (DT_STATE[bind] === undefined)
        DT_STATE[bind] = {};
      var bindType = this.$el.data('bind-type');
      if (bindType === 'start_ts')
        DT_STATE[bind]['start_ts'] = dtValue;
      else if (bindType === 'end_ts')
        DT_STATE[bind]['end_ts'] = dtValue;
    }
  },

  updateMinMaxForDtBind: function (name) {
    // TODO: refactor this - there is a lot of duplication setting things here and then on update as well
    // set initial binds
    var startMoment, endMoment;
    if (this.bind && DT_STATE[this.bind] && DT_STATE[this.bind]['end_ts'] && name === 'start_ts') {
      var maxDate = DT_STATE[this.bind]['end_ts'];
      this.$('.d-value').data("DateTimePicker").maxDate(maxDate);
      if (DT_STATE[this.bind]['start_ts']) {
        // there is start time
        startMoment = moment(DT_STATE[this.bind]['start_ts']);
        endMoment = moment(maxDate);
        if (endMoment.format('DD-MM-YYYY') === startMoment.format('DD-MM-YYYY')) {
          // we are in the same date, limit hours as well
          this.$('.t-value').data("DateTimePicker").maxDate(endMoment.format('HH:mm'));
        } else {
          // we are in different dates, remove maxDate limits
          this.$('.t-value').data("DateTimePicker").maxDate(false);
        }
      }
      this.$('.minmax').html("max: " + maxDate)
    } else if (this.bind && DT_STATE[this.bind] && DT_STATE[this.bind]['start_ts'] && name === 'end_ts') {
      var minDate = DT_STATE[this.bind]['start_ts'];
      this.$('.d-value').data("DateTimePicker").minDate(minDate);
      if (DT_STATE[this.bind]['end_ts']) {
        // there is end time
        endMoment = moment(DT_STATE[this.bind]['end_ts']);
        startMoment = moment(minDate);
        if (endMoment.format('DD-MM-YYYY') === startMoment.format('DD-MM-YYYY')) {
          // we are in the same date, limit hours as well
          this.$('.t-value').data("DateTimePicker").minDate(startMoment.format('HH:mm'));
        } else {
          // we are in different dates, remove minDate limits
          this.$('.t-value').data("DateTimePicker").minDate(false);
        }
      }
      this.$('.minmax').html("min: " + minDate)
    }
  },

  renderDt: function (name) {
    var dateOptions = {
      defaultDate: this.model.get('value').dt,
      format: 'YYYY-MM-DD',
      icons: DT_ICONS
    };
    var timeOptions = {
      defaultDate: this.model.get('value').dt,
      format: 'HH:mm',
      icons: DT_ICONS
    };

    // handle cases where name is start_ts or end_ts so that they are linked
    // https://eonasdan.github.io/bootstrap-datetimepicker/#linked-pickers
    if (name === 'end_ts') {
      dateOptions['useCurrent'] = false;
      timeOptions['useCurrent'] = false;
    }

    this.$('.d-value').datetimepicker(dateOptions);
    this.$('.t-value').datetimepicker(timeOptions);

    this.updateMinMaxForDtBind(name);

    // listen to the EVENT_BUS for events from either start_ts or end_ts
    this.listenTo(EVENT_BUS, 'updateAttributeDate', function (payload) {
      var from = payload.from;
      var to = this.model.get('name');
      if (from === to)
        return
      var dt = payload.dt;
      if (to === 'start_ts') {
        // this should set max date
        DT_STATE[this.bind]['end_ts'] = dt;
      } else if (to === 'end_ts') {
        // this should set min date
        DT_STATE[this.bind]['start_ts'] = dt;
      }
      this.updateMinMaxForDtBind(to);
    });
  }
});


var AppElementView = Backbone.View.extend({
  initialize: function (params) {
    this.options = params.options;
    this.name = params.name;
    this.template = _.template($("#fe-template-" + this.name + "-element").html());
    this.render();
  },

  events: function () {
    var defaultEvents = {
      'mouseover': 'highlightActions',
      'mouseout': 'deHighlightActions',
      'click .action-edit': 'initEdit',
      'click .action-delete': 'deleteEntry'
    };
    if (this.elementEvents) {
      defaultEvents = _.extend(defaultEvents, this.elementEvents)
    }
    return defaultEvents
  },

  highlightActions: function () {
    this.$('.highlightable').css('opacity', 1.0);
  },

  deHighlightActions: function () {
    this.$('.highlightable').css('opacity', 0);
  },

  deleteEntry: function () {
    this.model.destroy();
    this.model.destroy({data: {'request_sid': REQUEST_SID}, processData: true, ajaxSync: true});
    ApplicationRegistry.updateLatestUpdate(this.appName);
  },

  initEdit: function (e) {
    // display modal and bind the Edit View there
    var clickedElement = $(e.currentTarget);

    // get data from the clicked element
    var modelName = clickedElement.data('model-name');

    // initiate edit view
    new EditView({
      el: '#worldwide-modal',
      modelName: modelName,
      model: this.model
    });

    // opacity decrease to display selection
    this.$el.animate({
      opacity: '0.1'
    }, 200);

    $('#worldwide-modal').modal().on('shown.bs.modal', function (e) {

    }).on('hidden.bs.modal', function (e) {
      // restore opacity on close of modal
      this.$el.css('opacity', 1);
    }.bind(this))
  },

  beforeRender: function () {
    // no-op, override if needed
  },

  afterRender: function () {
    // no-op, override if needed
  },

  render: function () {
    this.beforeRender();
    createListElement(this);
    this.afterRender();
  }
});

var AppListView = Backbone.View.extend({
  initialize: function (params) {
    this.collectionName = params.collectionName;
    this.options = params.options;
    this.name = params.name;
    this.type = params.type;
    this.ElementViewClass = params.ElementViewClass;

    if (params.overrideUrl) {
      this.collection.url = params.overrideUrl;
      this.collection.fetch({ajaxSync: true});
    }

    if (params.filterCollection) {
      this.filterCollection = params.filterCollection;
    }

    this.listenTo(this.collection, 'sync change update', this.render);
    this.listenTo(NOTIFIER, 'server_created:' + this.collectionName, this.serverCreated);
    this.listenTo(NOTIFIER, 'server_updated:' + this.collectionName, this.serverUpdated);
    this.listenTo(NOTIFIER, 'server_deleted:' + this.collectionName, this.serverDeleted);
    this.template = _.template($("#fe-template-" + this.name + '-' + this.type).html());
    this.render();
  },

  events: function () {
    var defaultEvents = {
      'click .action-new': 'initNew'
    };
    if (this.listEvents) {
      defaultEvents = _.extend(defaultEvents, this.listEvents)
    }
    return defaultEvents
  },

  initNew: function (e) {
    // display modal and bind the Edit View there
    var clickedElement = $(e.currentTarget);

    // get data from the clicked element
    var modelName = clickedElement.data('model-name');

    // initiate edit view
    new NewView({
      el: '#worldwide-modal',
      modelName: modelName
    });
  },

  serverCreated: function (response) {
    this.collection.add(response['instance'], {at: 0});
    ApplicationRegistry.updateLatestUpdate(this.appName);
  },

  serverUpdated: function (response) {
    var _model = this.collection.findWhere({id: response.id});
    if (_model) {
      _model.set(response.updates);
      ApplicationRegistry.updateLatestUpdate(this.appName);
    }
  },

  serverDeleted: function (response) {
    this.collection.remove(response.id);
    ApplicationRegistry.updateLatestUpdate(this.appName);
  },

  filterCollection: function () {
    return this.collection.where({});
  },

  render: function () {
    var html = this.template();
    this.$el.html(html);
    this.$('.elements-list').html('');
    var collection = this.filterCollection();
    $.each(collection, function (_, model) {
      var item = new this.ElementViewClass({model: model, name: this.name});
      this.$('.elements-list').append(item.$el);
    }.bind(this));
    return this;
  }
});

var EditableInputModel = Backbone.Model.extend({
  defaults: {
    'name': null,
    'value': null,
    'placeholder': null,
    'realModel': null,
    'elDetails': {
      'type': 'str'
    }
  }
});

var ModalView = AppElementView.extend({
  initialize: function (params) {
    this.name = params.name;
    this.options = params.options;
    this.modelName = params.modelName;
    this.template = _.template($('#fe-template-edit-' + this.modelName).html());
    this.collectionClass = COLLECTIONS[this.modelName];
    this.modelClass = this.collectionClass.model;
    this.listenTo(EVENT_BUS, 'updateAttribute', this.onUpdate);
  },
  updatedAttributes: [],

  events: function () {
    var parentEvents = AppElementView.prototype.events(this);
    return _.extend(parentEvents, {
      "click button.action-save": "save",
      'keyup textarea': 'refreshAutoexpand',
      'enter textarea': 'save'
    });
  },

  _autoExpand: function (el) {
    setTimeout(function () {
      $(el).css('height', 'auto');
      $(el).css('height', el.scrollHeight + 60 + 'px');
    }, 0);
  },

  refreshAutoexpand: function (e) {
    var el = e.target;
    this._autoExpand(el)
  },

  render: function () {
    var html = this.template({data: {}});
    this.$('.modal-body').html(html);
    // need to redelegate events as it's a shared modal for both create and edit
    this.$el.off();
    this.delegateEvents();
    this.$('button.secondary-action').html('<i class="fa fa-trash"></i>').addClass('btn-danger');

    // render editable elements
    _.each(MODELS[this.modelName].elements, function (elDetails, elName) {
      if (this.$('.editable-' + elName).length) {
        createEditableElement(this, elName, 'Add ' + elName, elDetails);
      }
    }.bind(this));

    // hack to bind elements after they are rendered
    setTimeout(function () {
      _.each($('textarea'), function (el) {
        // set autoexpanding of textareas
        this._autoExpand(el);

        // set listeners for Enter key
        $(el).keyup(function (e) {
          if (e.keyCode == 13 && (e.shiftKey || e.ctrlKey)) {
            $(this).trigger('enter');
          }
        });
        // hack to bind elements after they are rendered
      }.bind(this));
    }.bind(this), 180);

    return this
  },

  onUpdate: function (attr) {
    if (this.updatedAttributes.indexOf(attr) === -1)
      this.updatedAttributes.push(attr);
  },

  save: function () {
    var updateAttrs = {};
    _.each(this.updatedAttributes, function (attr) {
      updateAttrs[attr] = this.model.get(attr);
    }.bind(this));
    this.store(updateAttrs);
    $('#worldwide-modal').modal('hide');
  }
});

var NewView = ModalView.extend({
  initialize: function (params) {
    ModalView.prototype.initialize(params);
    this.model = new this.modelClass();
    this.render();
  },

  events: function () {
    var parentEvents = ModalView.prototype.events(this);
    return _.extend(parentEvents, {});
  },

  store: function (updateAttrs) {
    this.collectionClass.createModel(this.model, updateAttrs);
  }
});

var EditView = ModalView.extend({
  initialize: function (params) {
    ModalView.prototype.initialize(params);
    if (this.model) {
      this.render();
    } else {
      this.listenToOnce(MODELS[params.modelName], 'sync', function (model) {
        this.model = model;
        this.render()
      });
    }
  },

  events: function () {
    var parentEvents = ModalView.prototype.events(this);
    return _.extend(parentEvents, {
      'click .action-delete': 'trash'
    });
  },

  store: function (updateAttrs) {
    this.model.saveAndPush(updateAttrs);
    ApplicationRegistry.updateLatestUpdate(this.appName);
  },

  trash: function () {
    this.deleteEntry();
    $('#worldwide-modal').modal('hide');
  }
});

var AppModel = Backbone.Model.extend({
  successHandler: function (model, response) {
    model.save(response.instance);
    // flash("Successfully created!", "success");
  },

  errorHandler: function (model, response) {
    // TODO: Revert adding the model to the collection
    if (response.Error)
      flash(response.Error, "danger");
    else
      flash("Something went wrong", "danger");
  },

  saveLocal: function () {
    this.save(); // local save
  },
  saveRemote: function (extraAttrs, extraOptions) {
    var attrs = _.extend(extraAttrs, {request_sid: REQUEST_SID});
    var options = _.extend({
      ajaxSync: true,
      success: this.successHandler,
      error: this.errorHandler
    }, extraOptions);
    this.save(attrs, options);
  },
  saveAndPush: function (attrs, extraOptions) {
    this.saveLocal();
    this.saveRemote(attrs, extraOptions)
  }
});

var AppCollection = Backbone.Collection.extend({
  initialize: function () {
    this.localStorage = new Backbone.LocalStorage(this.appName + '-collection');
  },

  /** create a model locally, save it to the server and add it to the collection */
  createModel: function (model, attrs, extraOptions) {
    this.add(model, extraOptions);
    model.saveRemote(attrs, extraOptions);
    ApplicationRegistry.updateLatestUpdate(this.appName);
  }
});

function handleReorderUpdates(collection, from, to) {
  /** Handles a reorder of elements in a collection - a rotation [from:to] */
  var toBeUpdated = [];
  var model, i;
  if (from < to) {
    // top-to-bottom move
    // handle a rotation between [from:to]
    for (i = from; i < to; i++) {
      if (i + 1 === to) {
        if (DEBUG)
          console.log(from + '->' + (to - 1));
        model = collection.at(from - 1);
        model.set('order', to - 1);
      } else {
        if (DEBUG)
          console.log(i + 1 + '->' + i);
        model = collection.at(i);
        model.set('order', i);
      }
      toBeUpdated.push(model)
    }
  } else {
    // bottom-to-top move
    // handle a rotation between [to:from]
    for (i = to; i <= from; i++) {
      if (i === from) {
        if (DEBUG)
          console.log(from + '->' + to);
        model = collection.at(from - 1);
        model.set('order', to);
      } else {
        if (DEBUG)
          console.log(i + '->' + (i + 1));
        model = collection.at(i - 1);
        model.set('order', i + 1);
      }
      toBeUpdated.push(model)
    }
  }
  // TODO: This should be optimised - save locally (.save()), but push deltas once (.customFunc())
  for (i = 0; i < toBeUpdated.length; i++) {
    toBeUpdated[i].saveAndPush();
  }
}


function createListElement(view) {
  var html = view.template({data: view.model.toJSON()});
  view.setElement(html);
}

function createEditableElement(view, name, placeholder, elDetails) {
  // Creates an editable input element
  value = view.model.get(name);
  if (value !== null && typeof value === "object")
    value = JSON.stringify(value);
  var model = new EditableInputModel({
    name: name,
    value: value,
    realModel: view.model,
    placeholder: placeholder,
    elDetails: elDetails
  });
  new EditableInputElement({
    model: model,
    el: view.$('.editable-' + name)
  });
}

function registerModels() {
  _.each(MODELS, function (model) {
    var modelObj = {
      urlRoot: '/api/' + model.namePlural,
      defaults: model.defaults
    };

    modelObj = _.extend(modelObj, model.modelMethods());
    var ConcreteModel = AppModel.extend(modelObj);

    var collectionObj = {
      appName: model.name,
      url: '/api/' + model.namePlural,
      model: ConcreteModel
    };

    collectionObj = _.extend(collectionObj, model.collectionMethods());
    var ConcreteCollection = AppCollection.extend(collectionObj);

    COLLECTIONS[model.name] = new ConcreteCollection();
  })
}

registerModels();

_.each(COLLECTIONS, function (value, key) {
  ApplicationRegistry.register(key, {
    'collection': value
  });
});